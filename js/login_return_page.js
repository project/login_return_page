(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.linkReturnPage = {
    attach: function (context) {
      var current_page = location.pathname,
        login_page = '/user/login';

      if (login_page != current_page) {
        /* This started as a custom module and we had special logic to
         * redirect to a authenticated-users-only page from a page for
         * anonymous users only; too niche to add a config option?
        if (current_page == '/members/map') {
          current_page = '/members/search';
        }
        */

        $(context).find("a[href='" + login_page + "']").once('login-processed-link').each(function () {
          $(this).attr('href', $(this).attr('href') + '?destination=' + current_page);
        });
      }
    }
  }
})(jQuery, Drupal);
